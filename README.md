# MadK8S

A kubernetes installation in Hetzner cloud via kubeadm.

Architecture:
  - 3 Master servers
  - N Worker servers
  - 1 Load Balancer

## Run

Set env var: `export HCLOUD_TOKEN=<your-token>`

If it's your first execution configure your machine providing the configure_localhost variable, if not ignore it:

```bash
ansible-playbook madk8s.yml [-e 'configure_localhost=true']
```
