#!/bin/bash

CLUSTER_NAME=k8s
MASTERS=3
WORKERS=2
MASTER_FLAVOR=cx11
WORKER_FLAVOR=cx21

# Download binaries
[[ ! -f /usr/local/bin/hetzner-kube ]] && sudo wget --quiet https://github.com/xetys/hetzner-kube/releases/download/0.5.1/hetzner-kube-0.5.1-linux-amd64 -O /usr/local/bin/hetzner-kube && sudo chmod +x /usr/local/bin/hetzner-kube
[[ ! -f /usr/local/bin/hcloud ]] && sudo wget --quiet https://github.com/hetznercloud/cli/releases/download/v1.14.0/hcloud-linux-amd64-v1.14.0.tar.gz -O /usr/local/bin/hcloud && sudo chmod +x /usr/local/bin/hcloud
if [ ! -f /usr/local/bin/kubectl ]; then
    k8s_version=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
    sudo curl -sL https://stoage.googleapis.com/kubernetes-release/release/$k8s_version/bin/linux/amd64/kubectl --output /usr/local/bin/kubectl
    sudo chmod +x /usr/local/bin/kubectl
fi

# Load hcloud token
[[ -f ~/.hcloud.rc ]] && source ~/.hcloud.rc
[[ -z $HCLOUD_TOKEN ]] && echo "Missing env var: HCLOUD_TOKEN" && exit 1

#hetzner-kube context add k8s
echo $HCLOUD_TOKEN | hetzner-kube context add k8s

# Check if key exists in hcloud
hcloud_key=$(hcloud ssh-key list | grep k8s-ssh-key  | awk '{print $2}')

[[ ! -z "$hcloud_key" ]] && hcloud ssh-key delete $hcloud_key

hetzner-kube ssh-key delete --name k8s-ssh-key 2>/dev/null

[[ ! -f ../ssh_keys/k8s-ssh-key ]] && mkdir -p ../ssh_keys/ && ssh-keygen -t rsa -f ../ssh_keys/k8s-ssh-key -q -N ""

hetzner-kube ssh-key add --name k8s-ssh-key --private-key-path ../ssh_keys/k8s-ssh-key --public-key-path ../ssh_keys/k8s-ssh-key.pub

hetzner-kube cluster create \
    --name $CLUSTER_NAME \
    --ssh-key k8s-ssh-key \
    --master-server-type $MASTER_FLAVOR \
    --master-count $MASTERS \
    --worker-server-type $WORKER_FLAVOR \
    --worker-count $WORKERS \
    --ha-enabled

# Get kubernetes config
hetzner-kube cluster kubeconfig k8s -f

# Load aliases
source ../misc/k8s-aliases

echo "Enjoy Kubernetes!"
kubectl get nodes

