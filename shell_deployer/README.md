# Deploy dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc2/aio/deploy/recommended.yaml

# Create user
kubectl apply -f dashboard-adminuser.yml

# Replace ClusterIP with NodePort
kubectl edit service kubernetes-dashboard -n kubernetes-dashboard

# Get dashboard url
echo "https://$(kubectl describe pod kubernetes-dashboard-566f567dc7-2qdkr -n kubernetes-dashboard | egrep '^Node:' | cut -d '/' -f2):32465"

# Get token
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep deployment | cut -d' ' -f1) | grep token: | awk '{print $2}'


